# -*- coding:utf-8 -*-
from flask import Flask, render_template, request
from flask.ext.pymongo import PyMongo
from flask.ext.mongoengine import MongoEngine
#~ from bson.objectid import ObjectId
from flask.ext.wtf import Form
from wtforms import TextField, IntegerField, SubmitField, HiddenField, RadioField,StringField,SelectField #formtype
from wtforms.validators import Required, ValidationError, InputRequired #驗證
import time #時間
import unicodedata
#~ MONGO_DBNAME = 'billorganizer'
DEBUG = True
CSRF_ENABLED = True
MONGODB_SETTINGS = {'DB':'billorganizer', 'HOST':'127.0.0.1', 'PORT':27017}
CSRF_ENABLED = True
SECRET_KEY = "askalsklakdslkdlks"
VERSION = '0.1'

app = Flask(__name__)
app.config.from_object(__name__)
#~ mongo = PyMongo(app)
db = MongoEngine(app)
get_dict = lambda dct, *keys: {key: dct[key] for key in keys}


class SpendForm(Form):
    #~ number = IntegerField(u'次數')
    item = SelectField(u'項目', choices=[(u'食', u'食'), (u'衣', u'衣'), 
    (u'住', u'住'),(u'行',u'行'),(u'雜',u'雜')])
    spend = IntegerField(u'花費')
    month = StringField(u'月')
    day = StringField(u'日')

    def validate_spend(self, field):
        if field.data == None:
            raise ValidationError(u'請輸入消費金額')
    
class Spend(db.Document):
    billorganizer = db.ListField(db.StringField())
   
    meta = {
        'collection': 'spend'
    }

    def get_id(self):
        return unicode(self.id)

@app.route('/', methods=['GET', 'POST'])
def spend():
    #~ spendlist=Spend()
    #~ all_spend = 0
    #~ spend_list=[]
    show_spend = Spend.objects
    sform=SpendForm()
    if sform.validate_on_submit():
        sform=SpendForm()

    time_n = time.strftime("%Y / %m / %d %H:%M",time.localtime(time.time()))
    time_month = time.strftime("%m",time.localtime(time.time()))
    time_day = time.strftime("%d",time.localtime(time.time()))    

    def dict2str(get_dict):
            this_list = get_dict.values()
            return this_list[0]
    
    if request.method == 'POST':
        howtodo = get_dict(request.form, 'action')
        if howtodo['action'] == u'add_spend':#新增部份
            #~ number_dict = get_dict(request.form, 'number')
            item_dict = get_dict(request.form, 'item')
            spend_dict = get_dict(request.form, 'spend')
            month_dict = get_dict(request.form, 'month')
            day_dict = get_dict(request.form,'day')

            billorganizer_list = [dict2str(month_dict), 
            dict2str(day_dict) ,dict2str(item_dict) ,
            dict2str(spend_dict) ]

            spend_dict['billorganizer'] = billorganizer_list #塞進billorganizer list

            ee=""
            for e in sform.spend.errors:
                if e :
                   ee+=e
 
            if ee == "":
                Spend(**spend_dict).save()

                
        if howtodo['action'] == u'del_spend':#刪除部份
            Spend(id=request.form["objid"]).delete()

    return render_template('Billorganizer.html',
    sform=sform,show_spend=show_spend,
    time_n=time_n,time_day=time_day,time_month=time_month,
    #~ all_spend=all_spend
    #~ ,new_number=new_number
        )
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
