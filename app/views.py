# --coding:utf-8 --
from flask import Flask, render_template, request
from app import app
from forms import SpendForm
from model import Spend
import time #時間

@app.route('/', methods=['GET', 'POST'])
def spend():
    #~ spendlist=Spend()
    #~ all_spend = 0
    #~ spend_list=[]
    show_spend = Spend.objects
    sform=SpendForm()

    time_n = time.strftime("%Y / %m / %d %H:%M",time.localtime(time.time()))
    time_month = time.strftime("%m",time.localtime(time.time()))
    time_day = time.strftime("%d",time.localtime(time.time()))    

    def dict2str(get_dict):
            this_list = get_dict.values()[0]
            return this_list[0]
    
    if request.method == 'POST':
        howtodo = get_dict(request.form, 'action')
        if howtodo['action'] == u'add_spend':#新增部份
            #~ number_dict = get_dict(request.form, 'number')
            if sform.validate_on_submit():
                sform=SpendForm()
                
            item_dict = get_dict(request.form, 'item')
            spend_dict = get_dict(request.form, 'spend')
            month_dict = get_dict(request.form, 'month')
            day_dict = get_dict(request.form,'day')

            billorganizer_list = [month_dict.values()[0], 
            day_dict.values()[0] ,item_dict.values()[0] ,
            spend_dict.values()[0] ]

            spend_dict['billorganizer'] = billorganizer_list #塞進billorganizer list

            #~ ee=""
            #~ for e in sform.spend.errors:
                #~ if e :
                   #~ ee+=e
#~ 
            #~ if ee == "":
                #~ Spend(**spend_dict).save()

            if sform.sped.errors == [] :
                Spend(**spend_dict).save()
            # try :
            #~  Spend(**spend_dict).save()
        if howtodo['action'] == u'del_spend':#刪除部份
            Spend(id=request.form["objid"]).delete()

    return render_template('Billorganizer.html',
    sform=sform,show_spend=show_spend,
    time_n=time_n,time_day=time_day,time_month=time_month,
    #~ all_spend=all_spend
    #~ ,new_number=new_number
        )
#~ if __name__ == '__main__':
    #~ app.run(debug=True,host='0.0.0.0')
