from flask import Flask
from flask.ext.mongoengine import MongoEngine

app = Flask(__name__)
app.config.from_object('config')

#~ app = Flask(__name__)
#~ app.config.from_object(__name__)
#~ mongo = PyMongo(app)
db = MongoEngine(app)
#~ get_dict = lambda dct, *keys: {key: dct[key] for key in keys}

from app import views
