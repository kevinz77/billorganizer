# --coding:utf-8 --

from app import db

class Spend(db.Document):
    billorganizer = db.ListField(db.StringField())
   
    meta = {
        'collection': 'spend'
    }

    def get_id(self):
        return unicode(self.id)
