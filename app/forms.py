# --coding:utf-8 --
from flask.ext.wtf import Form
from wtforms import TextField, IntegerField, SubmitField, HiddenField, RadioField,StringField,SelectField #formtype
from wtforms.validators import Required, ValidationError, InputRequired #驗證

class SpendForm(Form):
    #~ number = IntegerField(u'次數')
    item = SelectField(u'項目', choices=[(u'食', u'食'), (u'衣', u'衣'), 
    (u'住', u'住'),(u'行',u'行'),(u'雜',u'雜')])
    spend = IntegerField(u'花費')
    month = StringField(u'月')
    day = StringField(u'日')

    def validate_spend(self, field):
        if field.data == None:
            raise ValidationError(u'請輸入消費金額')
